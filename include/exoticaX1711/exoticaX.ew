-- exoticaX.ew v1.7.1
-- written by Chris Bensler
-- for use with EXOTICA1_7.dll (c)1999 - 2002 Todd Riggins
-- LAST MODIFIED : 03/11/02 / 13:25
---------------------------------------------------------------------------------

include exoticaX/exotica_api.ew
include exoticaX/ewin32api.ew

without warning

constant EX_VERSION = {1,7,1} -- {MAJOR,MINOR,REVISION}

-- Need HWND handle and HINSTANCE handle for EXOTICA use
integer hInst,hwnd

-- for shell_execute() routine (Thanks to Mark Honor of Liquid Nitrogen)
atom shell32   shell32 = open_dll( "shell32.dll" )
atom user32    user32  = open_dll( "user32.dll" )

if not shell32 then puts(1,"failed to open shell32.dll\n")   abort(1) end if
if not user32 then puts(1,"failed to open user32.dll\n")   abort(1) end if

constant    xShellExecute  = define_c_func(shell32, "ShellExecuteA",
		{C_LONG, C_LONG, C_LONG, C_LONG, C_LONG, C_LONG}, C_LONG)

constant    xFindWindow = define_c_func(user32,"FindWindowA",{C_LONG,C_LONG},C_LONG)
            
-- these are the various window settings to be used with shell_execute()
global constant SW_HIDE = 0,
		SW_SHOWNORMAL = 1,
		SW_NORMAL = 1,
		SW_SHOWMINIMIZED = 2,
		SW_SHOWMAXIMIZED = 3,
		SW_MAXIMIZE = 3,
		SW_SHOWNOACTIVATE = 4,
		--SW_SHOW = 5,  --Already in "ewin32api.ew"
		SW_MINIMIZE = 6,
		SW_SHOWMINNOACTIVE = 7,
		SW_SHOWNA = 8,
		SW_RESTORE = 9,
		SW_SHOWDEFAULT = 10,
		SW_MAX = 10

global atom HOT_MEDIA,HOT_STREAM,HOT_MIDI,HOT_CD
HOT_MEDIA=0    HOT_STREAM=0   HOT_MIDI=0  HOT_CD=0

-- For error messages
atom msg    msg = allocate(SIZE_OF_MESSAGE)

atom FPS_CALLED   FPS_CALLED=0

-- funcres holds returned function values
atom funcval

atom AActive            AActive=0
atom aActivated         aActivated=0

function WndProc(atom hwnd, atom iMsg, atom wParam, atom lParam)
   -- Here, the SetCursor is NULL so we wont see
   -- the windows mouse pointer during the program.
   if iMsg = WM_SETCURSOR then
	   funcval=c_func(SetCursor, {NULL})
	   return 0

   -- Here, we restore all surfaces and restore any
   -- video bitmaps when we alt-tab back to an EXOTICA program
   -- by using the DDRestore_Video_Bitmaps function.
   elsif iMsg = WM_ACTIVATEAPP then
	   AActive = wParam
	   aActivated = 0
	   if AActive then
	      if c_func(IS_DDRAW_VALID, {}) then
	         c_proc(REAQUIRE_INPUT, {})
   	      c_proc(RESTORE_VIDEO_BITMAPS, {})
	         AActive = 2
	         aActivated=1
   	   end if
	   end if
	   return 0

   -- WM_KEYDOWN is here for me to help me test my programs.
   -- you can use it too, add more virtual keys if you want.
   -- But when I start programming games with EXOTICA, I will use
   -- Euphoria's DirectInput for key control most likely.
   -- Also, here, pressing the ESCAPE key during the program will
   -- post the WM_DESTROY message to tell it's time to end the program.
   elsif iMsg = WM_KEYDOWN then
      if wParam = VK_ESCAPE then
         funcval=c_func(PostMessage, {hwnd, WM_DESTROY, wParam, lParam})
         return 0
      end if

   -- Here, WM_DESTROY tells the program to end.
   elsif iMsg = WM_DESTROY then
	   c_proc(PostQuitMessage, {0})
	   return 0
   end if

   return c_func(DefWindowProc, {hwnd, iMsg, wParam, lParam})
end function

--v1.2.1
global function exotica_get_handle()
   return hwnd
end function

--v1.2.1
global function exotica_get_instance()
   return hInst
end function

--v1.2.1
sequence IconName    IconName=""
global procedure set_icon(sequence Icon_Name)
   IconName=Icon_Name
end procedure

global procedure exoticax_deactivate()
   c_proc(ShowWindow, {hwnd,SW_MINIMIZE})
end procedure

global procedure exoticax_reactivate()
   c_proc(ShowWindow, {hwnd, SW_SHOWMAXIMIZED})
   c_proc(UpdateWindow,{hwnd})
end procedure

---------------------------------------------------------------------
-- DEBUG ROUTINES --

global function exotica_error()
   AActive = aActivated
   if c_func( PeekMessage, { msg, NULL, 0, 0, PM_NOREMOVE } ) then
      if c_func( GetMessage, { msg, NULL, 0, 0 } )=0 then
   	   return 1
      end if
      c_proc( TranslateMessage, { msg } )
      c_proc( DispatchMessage, { msg } )
   end if
   FPS_CALLED=0
   return c_func(ON_ERROR,{})
end function

-- aActive tells the program if the program is in focuse or not.
-- ie.. if you alt-tab out of the program, aActive=0
--      if you alt-tab back into the program, aActive=2
--           until the program loops once, then aActive=1
global function aActive()
   return AActive
end function

global procedure aActive_wait()
 while not AActive do
    funcval = exotica_error()
 end while
end procedure

global procedure report_mode(sequence report_file,integer debug_full)
atom tmp
tmp = allocate_string(report_file)
 c_proc(REPORT_MODE,{tmp, debug_full})
free(tmp)
end procedure

global procedure file_report(sequence report_DATA)
atom tmp
tmp = allocate_string(report_DATA)
 c_proc(FILE_REPORT,{tmp})
free(tmp)
end procedure

global procedure error_message(sequence message_DATA)
atom tmp
tmp = allocate_string(message_DATA)
 c_proc(ERROR_MESSAGE,{tmp})
free(tmp)
 file_report(message_DATA)
end procedure

---------------------------------------------------------------------
-- EXOTICA INTER-LIBRARY ROUTINES --
-- These routines are only for use by the library, not for user use
sequence EXOTICAX_IDS    EXOTICAX_IDS={-1,-1,-1,-1}
sequence IS_INITX        IS_INITX={0,0,0,0}

global procedure Xset_exoticaX_id(atom R, atom ID)
   EXOTICAX_IDS[R] = ID
end procedure

global function Xget_exoticaX_id(atom R)
   return EXOTICAX_IDS[R]
end function

global procedure Xset_is_initX(atom R,atom V)
   IS_INITX[R]=V
end procedure

global function Xget_is_initX(atom R)
   return IS_INITX[R]
end function

---------------------------------------------------------------------
-- EXOTICA EXIT ROUTINES --
global procedure exotica_exit()
IS_INITX={0,0,0,0}
 if HOT_MEDIA then
    if HOT_MEDIA = 2 then
       c_proc(CLOSE_MEDIA,{})
    end if
    c_proc(CLOSE_MEDIA_WINDOW,{})
 end if
 if HOT_STREAM then c_proc(STREAM_BUFFER_EXIT,{}) end if
 if HOT_MIDI then
    c_proc(MIDI_STOP,{})
    c_proc(MIDI_CLOSE,{})
 end if
 if HOT_CD then
    c_proc(CD_STOP,{})
    c_proc(CD_CLOSE,{})
 end if
 c_proc(EXOTICA_EXIT,{})
end procedure

global procedure exotica_abort(atom error_code)
   file_report(sprintf("   EXOTICA_ABORT (error_code: %d)    ",error_code))
   exotica_exit()
   abort(error_code)
end procedure

---------------------------------------------------------------------
-- EXOTICA INIT ROUTINES --

sequence app_title      app_title = "ExoticaX Application" -- DEFAULT
atom duplicate_allowed  duplicate_allowed=0

global procedure set_app_title(sequence title)
   app_title = title
end procedure

global procedure allow_duplicates()
   duplicate_allowed=1
end procedure

global procedure exotica_init()
 atom szAppName,szAppTitle,wndclass,WndProcAddress,class,tmpIconName
 integer id

 -- Must get the instance handle of the application
 hInst = instance()

 wndclass = allocate(SIZE_OF_WNDCLASS)
 -- allocate a message buffer
 szAppName = allocate_string("EXOTICAX")
 szAppTitle = allocate_string(app_title)
 
 if length(IconName) then tmpIconName=allocate_string(IconName) end if
 
 if not duplicate_allowed then
   if c_func(xFindWindow,{szAppName,szAppTitle}) then abort(-3) end if
 end if
 
 id = routine_id("WndProc")
 if id = -1 then
   puts(1, "routine_id failed!\n")
   abort(-2)
 end if
 WndProcAddress = call_back(id) -- get 32-bit address for callback

 poke4(wndclass + cbSize, SIZE_OF_WNDCLASS)
 poke4(wndclass + style, or_bits(CS_HREDRAW, CS_VREDRAW))
 poke4(wndclass + lpfnWndProc, WndProcAddress)
 poke4(wndclass + cbClsExtra, 0)
 poke4(wndclass + cbWndExtra, 0)
 poke4(wndclass + hInstance, hInst)
 poke4(wndclass + hIcon, NULL)
 poke4(wndclass + hCursor, NULL)
 poke4(wndclass + hbrBackground, c_func(GetStockObject, {NULL_BRUSH}))
 poke4(wndclass + lpszMenuName, NULL)
 poke4(wndclass + lpszClassName, szAppName)
 poke4(wndclass + hIconSm, NULL)

 if length(IconName) then free(tmpIconName) end if

 class = c_func(RegisterClassEx, {wndclass})
 if class = 0 then
    puts(1, "Couldn't register class\n")
    while get_key()=-1 do end while
    abort(-2)
 end if
 hwnd = c_func(CreateWindow, {
		    WS_EX_TOPMOST,           -- extended style
		    szAppName,               -- window class name
		    szAppTitle,              -- window caption
		    or_bits(WS_POPUP,WS_SYSMENU),   -- window style
		    0,                       -- initial x position
		    0,                       -- initial y position
		    c_func(GetSystemMetrics,{SM_CXSCREEN}), -- initial x size
		    c_func(GetSystemMetrics,{SM_CYSCREEN}), -- initial y size
		    NULL,                    -- parent window handle
		    NULL,                    -- window menu handle
		    hInst,                   --hInstance // program instance handle
		    NULL})              -- creation parameters
 if hwnd = 0 then
    puts(1, "Couldn't CreateWindow\n")
    abort(-2)
 end if
 c_proc(ShowWindow, {hwnd, SW_SHOW})
 c_proc(UpdateWindow, {hwnd})

 if c_func(EXOTICA_INIT,{hwnd,hInst}) then
    error_message("EXOTICA_INIT")
    exotica_abort(-1)
 end if
 file_report("-- DEBUG FILE for " & app_title & " --")
 
 AActive=2
 aActivated=1

end procedure

sequence DSOUND_DEFAULTS   DSOUND_DEFAULTS={DSSCL_EXCLUSIVE,2,44100,16}

global procedure set_dsound(atom mode,atom chans,atom freq,atom bps)
   DSOUND_DEFAULTS={mode,chans,freq,bps}
end procedure

global procedure exotica_initX(atom scrX, atom scrY, atom bpp)
   exotica_init()
   if EXOTICAX_IDS[1] != -1 then
      if call_func(EXOTICAX_IDS[1],{scrX,scrY,bpp}) then exotica_abort(-20) end if
   end if
   if EXOTICAX_IDS[2] != -1 then
      if call_func(EXOTICAX_IDS[2],{}) then exotica_abort(-21) end if
   end if
   if EXOTICAX_IDS[3] != -1 then
      if call_func(EXOTICAX_IDS[3],DSOUND_DEFAULTS) then exotica_abort(-22) end if
   end if
   if EXOTICAX_IDS[4] != -1 then
      if call_func(EXOTICAX_IDS[4],{}) then exotica_abort(-23) end if
   end if
end procedure

------------------------------------------------------------------------
-- MEMORY ROUTINES

global function total_video_memory()
   return c_func(TOTAL_VIDEO_MEMORY,{})
end function

global function avail_video_memory()
   return c_func(AVAIL_VIDEO_MEMORY,{})
end function

global function total_texture_memory()
   return c_func(TOTAL_TEXTURE_MEMORY,{})
end function

global function avail_texture_memory()
   return c_func(AVAIL_TEXTURE_MEMORY,{})
end function

---------------------------------------------------------------------
-- WIN ROUTINES --

global function winver()
   return c_func(WINVER,{})
end function

global procedure shell_execute(sequence cmd, sequence file, sequence params, atom style)
-- call ShellExecute to display a file
atom junk, file_string, cmd_string,params_string
   params_string = NULL
   -- convert to strings
   cmd_string = allocate_string( cmd )
   file_string = allocate_string( file )
   if not equal("",params) then params_string = allocate_string( params ) end if
   -- call ShellExecute
   junk = c_func( xShellExecute,
	   { hwnd, cmd_string, file_string, params_string, 0, style } )
   -- free the strings
   if not equal("",params) then free( params_string ) end if
   free( cmd_string )
   free( file_string )
end procedure

global function exoticax_app_exists(sequence Title)
atom lpClass,lpTitle,retVal
   lpClass = allocate_string("EXOTICAX")
   lpTitle = allocate_string(Title)
   retVal = c_func(xFindWindow,{lpClass,lpTitle})
   free(lpClass)
   free(lpTitle)
 return retVal
end function

------------------------------------------------------------------------
-- FRAMESPEED ROUTINES

-- variable helpers for getting Frames Per Second
atom FrameTime,Frames,FrameCount,fTime,GameTime
global atom EXOTICAX_FRAMES_PER_SECOND    EXOTICAX_FRAMES_PER_SECOND=0
------------------------------------------------------
-- Set FrameTime and GameTime to current clock ticks
------------------------------------------------------
global function framerate(atom fps)
   if not FPS_CALLED then
      if EXOTICAX_FRAMES_PER_SECOND then
         while c_func( timeGetTime,{}) - GameTime < 1000/fps do end while
         FrameCount+=1
         fTime = c_func( timeGetTime,{}) - FrameTime
         if fTime >= 1000 then
            Frames = ( FrameCount*1000 )/fTime
            FrameTime = c_func( timeGetTime,{})
            FrameCount = 0
         end if
         GameTime = c_func( timeGetTime,{})
      else
         EXOTICAX_FRAMES_PER_SECOND=fps
         FrameTime = c_func( timeGetTime, {} )
         GameTime = c_func( timeGetTime, {} )
         Frames = fps
         FrameCount =0
         fTime = 0
      end if
      FPS_CALLED=1
   end if
   return Frames
end function

------------------------------------------------------------------------
-- v1.7.1 *NEW*
-- Report the library version
global function exoticaX_version()
   return EX_VERSION -- {MAJOR,MINOR,REVISION}
end function

--v1.7.1 *NEW* (for internal use)
global procedure verify_exoticaX_library(sequence version, sequence lib_file)
   if not equal(version,EX_VERSION) then
      puts(1,sprintf("%s : EXPECTED exoticaX.ew v%d.%d.%d",{lib_file}&version)&"\r\n")
      while get_key() =-1 do end while
      abort(1)
   end if
end procedure