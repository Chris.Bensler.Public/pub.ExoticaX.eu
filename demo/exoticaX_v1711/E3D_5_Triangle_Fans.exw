--
-- Exotica 3D Example -- By: Todd A.Riggins
--

include exoticaX/exoticaX.ew
include exoticaX/ddrawX.ew
include exoticaX/dinputX.ew
include exoticaX/d3dX.ew

set_app_title("E3D_5_Triangle_Fans Demo for ExoticaX v1.7.1")
report_mode("E_Report",1)

constant FPS=60 -- FrameSpeed Lock
constant s_x=640, s_y=480 -- screen width & height

sequence vnormal  -- vector normal
atom Vertices
Vertices=allocate(192) -- 8 Members of the Vertex * 4 bytes each * 6 Vertices

------------------------------------------------------------------------------
-- INIT

   exotica_init()

   -- Set up Direct Draw
   if ddraw_init(s_x, s_y, 16) then exotica_abort(1) end if

   -- Setup Direct3D
   if d3d_init() then exotica_abort(3) end if
   if create3d_device(0,0) then exotica_abort(4) end if
   if create_viewport(0,0,s_x,s_y,0.0,1.0) then exotica_abort(5) end if

   -- Setup DirectInput
   if dinput_init() then exotica_abort(6) end if
   if keyboard_init(0) then exotica_abort(7) end if

   vnormal = { 0.0, 0.0, -1.0 }

 -- Initialize the 3 vertices for the front of the triangle
   Vertices=poke_floats(Vertices,0*(8),{ 0.0, 0.0, 0.0} & vnormal & {0.0,0.0})
   Vertices=poke_floats(Vertices,1*(8),{-5.0, 5.0, 0.0} & vnormal & {0.0,0.0})
   Vertices=poke_floats(Vertices,2*(8),{-3.0, 7.0, 0.0} & vnormal & {0.0,0.0})
   Vertices=poke_floats(Vertices,3*(8),{ 0.0,10.0, 0.0} & vnormal & {0.0,0.0})
   Vertices=poke_floats(Vertices,4*(8),{ 3.0, 7.0, 0.0} & vnormal & {0.0,0.0})
   Vertices=poke_floats(Vertices,5*(8),{ 5.0, 5.0, 0.0} & vnormal & {0.0,0.0})

 -- Set the material as yellow. We're setting the ambient color here
 -- since this tutorial only uses ambient lighting. For apps that use real
 -- lights, the diffuse and specular values should be set. (In addition, the
 -- polygons' vertices need normals for true lighting.)
   material=Set_Ambient_Material(material, 1.0, 1.0, 0.0, 0.0)
   if set_materials(material) then exotica_abort(8) end if
   -- The ambient lighting value is another state to set. Here, we are turning
   -- ambient lighting on to full white.
   if rs_set_ambient_color(make_rgb({255,255,255})) then exotica_abort(9) end if

   if set_transform_view(matWorld) then exotica_abort(10) end if
   -- The view matrix defines the position and orientation of the camera.
   -- Here, we are just moving it back along the z-axis by 50 units.
   matView = poke_floats(matView,14,{50.0}) -- _43
   if set_transform_view(matView) then exotica_abort(11) end if

   -- The projection matrix defines how the 3D scene is "projected" onto the
   -- 2D render target (the backbuffer surface). Refer to the docs for more
   -- info about projection matrices.
   matProj = poke_floats(matProj,0,{2.0}) -- _11
   matProj = poke_floats(matProj,5,{2.0}) -- _22
   if set_transform_projection(matProj) then exotica_abort(12) end if

------------------------------------------------------------------------------
-- MAIN

-- Exit program by hitting the escape key
while not keyboard_keystate(DIK_ESCAPE) do
if aActive() then

   -- Update Direct Input for Immediate data(ie:mouse,jouystick,keyboard)
   dinput_update()

   if clear_viewport(D3DCLEAR_TARGET, {0,0,65535}, 0.0) then exotica_abort(14) end if

   if begin_scene() then exotica_abort(15) end if

   -- Draw the triangle using a DrawPrimitive() call. Subsequent
   -- tutorials will go into more detail on the various calls for
   -- drawing polygons.
   if draw_primitive(D3DPT_TRIANGLEFAN, D3DFVF_VERTEX, Vertices, 6) then
      exotica_abort(16)
   end if

   if end_scene() then exotica_abort(17) end if

   if gdi_textout(5,5,sprintf("FPS : %03d",framerate(FPS)),{255,255,255},1) then
      exotica_abort(18)
   end if

   --Flip the back buffer surface to the Primary surface to
   --let us see the updated drawings
   if surface_flip() then exotica_abort(19) end if

end if -- aActive END
   if exotica_error() then exotica_abort(20) end if
end while

------------------------------------------------------------------------------
-- EXIT
exotica_exit()

