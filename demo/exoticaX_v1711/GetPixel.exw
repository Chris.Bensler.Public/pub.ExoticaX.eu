   --
-- EXOTICA GetPixel Example - By: Todd Riggins
--
-- This example demostrates mainly the GET_PIXEL, GET_RED_VALUE,
-- GET_GREEN_VALUE and the GET_BLUE_VALUE routines.
--
-- Note: You must comment out the LOAD_PALETTE routine if
-- you want to test this example in a higher Depth( 16,24 or 32).
without warning

include exoticaX/exoticaX.ew
include exoticaX/ddrawX.ew
include exoticaX/dinputX.ew

set_app_title("GetPixel Demo for ExoticaX v1.7.1")
report_mode("E_Report",1)

sequence Rect   Rect = {0,0,0,0}

-- Mouse Vars
-- stores x,y mouse movements
atom mx     mx = 320
atom my     my = 240

-----------------------------------------------------------------------------
-- Init

 -- Initialize Exotica
 exotica_init()

 -- Set up Direct Draw
 if ddraw_init(640,480,16) then exotica_abort(1) end if

 -- Setup DirectInput
 if dinput_init() then exotica_abort(1) end if

 -- Setup DirectInput's Mouse
 if mouse_init() then exotica_abort(1) end if

 -- Load palette if video is in 8bit depth
 if surface_depth()=8 then
    if load_palette("bmp/arrow.bmp") then exotica_abort(1) end if
 end if

 -- Load 'arrow' bitmap 16x16 into video memory with flag set as 1
constant ARROW =load_bitmap("bmp/arrow.bmp",1)
 if not ARROW then exotica_abort(1) end if

 -- set the transparency to black for the mouse arrow
 -- (redundant, it is default)
 set_bitmap_trans_color(ARROW,0)

-----------------------------------------------------------------------------
-- Main

atom col
atom red        red     = 0
atom green      green   = 0
atom blue       blue    = 0
atom mrgb_val   mrgb_val= 0
atom pixcol     pixcol  = 0
atom txtcol     txtcol  = 255
if surface_depth()!=8 then txtcol=65535 end if

while 1 do
if aActive() then
 -- Update Direct Input for Immediate data(ie:mouse,jouystick,keyboard)
 dinput_update()

 -- Clear Surface to palette color 0
 if clear_surface(pixcol) then exotica_abort(1) end if

 -- Draw the palette colors on screen
 col=0
 for j=0 to 7 do
    for i=0 to 31 do
       Rect[1] = (j*18)+260
       Rect[2] = 300+(i*4)
       Rect[3] = (j*18)+275
       Rect[4] = 304+(i*4)
       if surface_depth() = 8 then
          if surface_rect_blit(Rect,col) then exotica_abort(1) end if
       else
          if surface_rect_blit(Rect,{col,col,col}) then exotica_abort(1) end if
       end if
       col=col+1
    end for
 end for

 -- Get Mouse Data
 mx +=mouse_x()
 my +=mouse_y()

 -- Keep Mouse Pointer(In this case the Arrow) inside the screen
 if mx<0 then mx=0 end if
 if mx>(640-1) then mx=(640-1) end if
 if my<0 then my=0 end if
 if my>(480-1) then my=(480-1) end if

 -- Must Lock surface before you put any pixels
 if surface_lock() then exotica_abort(1) end if

 -- Get Pixel Color
 if mx > 259 and mx < 402 and my > 299 and my < 561 then
    if surface_depth()=8 then
       pixcol = pal_get_pixel(mx,my)
       if pixcol=4 or pixcol=15 or pixcol=64 or pixcol=255 then txtcol=0 else txtcol=255 end if
    else
       pixcol = rgb_get_pixel(mx,my)
       red   = get_red_value(pixcol)
       green = get_green_value(pixcol)
       blue  = get_blue_value(pixcol)
       mrgb_val = make_rgb({red,green,blue})
       if pixcol < 32767 then txtcol=65535 else txtcol=0 end if
    end if
 end if

 -- Now unlock the surface after you plot yer pixels.
 if surface_unlock() then exotica_abort(1) end if

 if gdi_textout_center(320,20,"POINT TO DIFFERENT COLORS",txtcol,1) then exotica_abort(1) end if
 if gdi_textout_center(320,32,"TO RETURN THEIR RGB VALUES",txtcol,1) then exotica_abort(1) end if
 if gdi_textout_center(320,44,"PRESS [ESC] KEY TO EXIT",txtcol,1) then exotica_abort(1) end if

 if gdi_textout(260,188, sprintf("%dBit Pixel",surface_depth()),txtcol,1) then
    exotica_abort(1)
 end if
 if gdi_textout(260,200,sprintf("Color Value: %d", pixcol),txtcol,1) then
    exotica_abort(1)
 end if

 if surface_depth()>8 then
    if gdi_textout(260,224,sprintf("Red Value   : %d", red),txtcol,1) then
       exotica_abort(1)
    end if
    if gdi_textout(260,236,sprintf("Green Value: %d", green),txtcol,1) then
       exotica_abort(1)
    end if
    if gdi_textout(260,248,sprintf("Blue Value  : %d", blue),txtcol,1) then
       exotica_abort(1)
    end if
    if gdi_textout(260,260,sprintf("MAKE_RGB Value : %d",mrgb_val),txtcol,1) then
       exotica_abort(1)
    end if
 end if

 -- Blit ARROW as a transparent sprite
 if trans_blitfast(ARROW, mx, my) then exotica_abort(1) end if

 --Flip the surface
 if surface_flip() then exotica_abort(1) end if

end if -- aActive
 -- This polls for windows messages and any errors that might've occurred.
 -- **NOTE**  THIS SHOULD ALWAYS BE CALLED
 if exotica_error() then exotica_abort(1) end if
end while

-- Shutdown EXOTICA
exotica_exit()

