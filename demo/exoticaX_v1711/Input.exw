--
-- EXOTICA DirectInput Example - By: Todd Riggins
--
-- This example demostrates DirectInput's Keyboard, mouse and joystick routines.
--
include exoticaX/exoticaX.ew
include exoticaX/ddrawX.ew
include exoticaX/dinputX.ew

set_app_title("Input Demo for ExoticaX v1.7.1")
report_mode("E_Report",1)

constant s_x=640,s_y=480

object Text_Col   Text_Col = {255,0,0}

-----------------------------------------------------------------------------
-- Init

   -- Initailize Exotica
   exotica_init()

   -- Set up Direct Draw
   if ddraw_init(s_x,s_y,16) then exotica_abort(1) end if

   if surface_depth()=8 then
      -- Load Palette from coolman.bmp
      if load_palette("bmp/coolman.bmp") then exotica_abort(1) end if
      -- Set the text color to red from the palette
      Text_Col = 254
   end if

   -- Load 'coolman' bitmap into video memory with flag set as 1
   constant COOLMAN =load_bitmap("bmp/coolman.bmp",1)
   if not COOLMAN then exotica_abort(1) end if

   -- Setup DirectInput 
   if dinput_init() then exotica_abort(1) end if

   -- Setup DirectInput's Keyboard Immediate Mode
   if keyboard_init(0) then exotica_abort(1) end if
   -- Setup DirectInput's Mouse
   if mouse_init() then exotica_abort(1) end if
   -- Setup DirectInput's Joystick
   if joystick_init() then exotica_abort(2) end if

   joy_x_range(-5000,5000)   
   joy_x_deadzone(1000)
   joy_x_saturation(9000)
   joy_y_range(-5000,5000)   
   joy_y_deadzone(1000)
   joy_y_saturation(9000)
   joy_z_range(-1000,1000)   
   joy_z_deadzone(1000)
   joy_z_saturation(9000)
   joy_rx_range(-1000,1000)   
   joy_rx_deadzone(1000)
   joy_rx_saturation(9000)
   joy_ry_range(-1000,1000)   
   joy_ry_deadzone(1000)
   joy_ry_saturation(9000)
   joy_rz_range(-1000,1000)   
   joy_rz_deadzone(1000)
   joy_rz_saturation(9000)
   joy_slider_range(0,-1000,1000)
   joy_slider_range(1,-1000,1000)
      
-----------------------------------------------------------------------------
-- Main

-- Mouse Vars
   atom mx, my, mz      -- stores x,y,z[Wheel] mouse movements  
   atom mb0,mb1,mb2,mb3 -- stores mouse button status
   atom mb4,mb5,mb6,mb7 -- stores mouse button status

   mx = 320-64
   my = 300
   mz = 0

-- Joystick Vars
   atom xa, ya, za, rxa, rya, rza, s0, s1, p0, p1, p2, p3
   xa=0
   ya=0
   za=0
   rxa=0
   rya=0
   rza=0
   s0=0
   s1=0
   p0=0
   p1=0
   p2=0
   p3=0


-- Exit program by hitting the escape key
while not keyboard_keystate(DIK_ESCAPE) do
-- If user alt-tabs out to windows with the program running, pause everthying.
-- When user alt-tabs back into program, unpause everything. The 'aActive' is
-- defined in 'emain.ew' as a global atom which handles this situation.
if aActive() then

   -- Update Direct Input for Immediate data(ie:mouse,jouystick,keyboard)
   dinput_update()

   -- Clear Surface to palette color 0
   if clear_surface(0) then exotica_abort(1) end if

   --- Get Mouse Data
   mx +=mouse_x()
   my +=mouse_y()
   mz +=mouse_z()
   mb0=mouse_b0()
   mb1=mouse_b1()
   mb2=mouse_b2()
   mb3=mouse_b3()
   mb4=mouse_b4()
   mb5=mouse_b5()
   mb6=mouse_b6()
   mb7=mouse_b7()

   -- Keep Mouse Pointer(In this case the Happy face 'COOLMAN' image) inside the screen
   if mx<0 then mx=0 end if
   if mx>(s_x-32) then mx=(s_x-32) end if
   if my<0 then my=0 end if
   if my>(s_y-32) then my=(s_y-32) end if

   --- Print Mouse Data
   if gdi_textout_center(470,20,"[DATA FROM MOUSE]",Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,38,sprintf("Mouse X:%d",mx),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,56,sprintf("Mouse Y:%d",my),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,74,sprintf("Mouse Z:%d",mz),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,92,sprintf("Mouse Button 0 = %d", mb0),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,110,sprintf("Mouse Button 1 = %d", mb1),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,128,sprintf("Mouse Button 2 = %d", mb2),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,146,sprintf("Mouse Button 3 = %d", mb3),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,164,sprintf("Mouse Button 4 = %d", mb4),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,182,sprintf("Mouse Button 5 = %d", mb5),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,200,sprintf("Mouse Button 6 = %d", mb6),Text_Col,1) then exotica_abort(1) end if
   if gdi_textout_center(470,218,sprintf("Mouse Button 7 = %d", mb7),Text_Col,1) then exotica_abort(1) end if

   if not joystick_detected() then
      if gdi_textout(200,116,"[NO JOYSTICK DETECTED]",Text_Col,1) then exotica_abort(1) end if
   else
      -- reading joystick data here
      xa= joy_x_axis()
      if xa then mx+=xa/1000.0 end if
      ya=joy_y_axis()
      if ya then my+=ya/1000.0 end if
      za=joy_z_axis()
      rxa=joy_rx_axis()
      rya=joy_ry_axis()
      rza=joy_rz_axis()
      s0=joy_slider(0)
      s1=joy_slider(1)
      p0=joy_pov(0)
      p1=joy_pov(1)
      p2=joy_pov(2)
      p3=joy_pov(3)

      --- Print Joystick Data
      if gdi_textout(200,20,"[DATA FROM JOYSTICK]",Text_Col,1) then exotica_abort(1) end if

      if gdi_textout(240,38,sprintf("X Axis = %d", xa),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,56,sprintf("Y Axis = %d", ya),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,74,sprintf("Z Axis = %d", za),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,92,sprintf("Rx Axis = %d", rxa),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,110,sprintf("Ry Axis = %d", rya),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,128,sprintf("Rz Axis = %d", rza),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,146,sprintf("Slider 0 = %d", s0),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,164,sprintf("Slider 1 = %d", s1),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,182,sprintf("Pov Hat 0 = %d", p0),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,200,sprintf("Pov Hat 1 = %d", p1),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,218,sprintf("Pov Hat 2 = %d", p2),Text_Col,1) then exotica_abort(1) end if
      if gdi_textout(240,236,sprintf("Pov Hat 3 = %d", p3),Text_Col,1) then exotica_abort(1) end if

      -- Print what joystick buttons that are being pressed
      for i=0 to 31 do
         if joy_button(i) then
            if gdi_textout(240,254+(i*12),sprintf("BUTTON # %d", i),Text_Col,1) then
               exotica_abort(1)
            end if
         end if
      end for  
   end if

   -- Check and Print what Selected Keys are being pressed
   if gdi_textout(20,194,"[DATA FROM KEYBOARD]",Text_Col,1) then exotica_abort(1) end if
   if gdi_textout(20,208,"PRESS THE ARROWS ON",Text_Col,1) then exotica_abort(1) end if
   if gdi_textout(20,222,"THE ARROW KEYPAD",Text_Col,1) then exotica_abort(1) end if
   if keyboard_keystate(DIK_UP) then
      my-=5
      if gdi_textout(20,240,"Up Arrow on arrow keypad",Text_Col,1) then exotica_abort(1) end if
   elsif keyboard_keystate(DIK_DOWN) then
      my+=5
      if gdi_textout(20,252,"Down Arrow on arrow keypad",Text_Col,1) then exotica_abort(1) end if
   elsif keyboard_keystate(DIK_LEFT) then
      mx-=5
      if gdi_textout(20,264,"Left Arrow on arrow keypad",Text_Col,1) then exotica_abort(1) end if
   elsif keyboard_keystate(DIK_RIGHT) then
      mx+=5
      if gdi_textout(20,276,"Right Arrow on arrow keypad",Text_Col,1) then exotica_abort(1) end if
   end if

   -- Blit coolman as a transparent sprite
   if trans_blitfast(COOLMAN,mx,my) then exotica_abort(1) end if

   --Flip the surface
   if surface_flip() then exotica_abort(1) end if
-- aActive end
end if
   -- This polls for windows messages and any errors that might've occurred.
   -- **NOTE**  THIS SHOULD ALWAYS BE CALLED
   if exotica_error() then exotica_abort(1) end if
end while

-- Shutdown EXOTICA.
exotica_exit()

